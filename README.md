# BBWorker

Officially maintained client implementation for doing work on the Block Bounty platform.  This package can be used as a CLI or be required in your own projects.

## CLI

1. Get started with an install!

    ```
    npm install -g bbworker
    ```

1. Usage
    ```
    Usage: bbworker <command> [options]

    Commands:
    bbwork new   start a new job, returns the job id
    bbwork work  start working on an existing job

    Options:
    --version     Show version number  [boolean]
    -s, --server  Optional parameter to specify the api server to use  [string] [default: "http://localhost:8089"]
    -h, --help    Show help  [boolean]

    Examples:
    bbwork new -f wasm.wasm -c config.json [-s http://localhost:8089]               start a new job, returns the job id
    bbwork work -a 0x0 -j 1 [-s http://localhost:8089]  start working on an existing job

    Please specify one of the commands!
    ```

Alternatively skip the install entirely!
```
npx bbworker new -f wasm.wasm -c config.json
```

## Code

*To Be Documented*

For now feel free to check out the source in cli.ts to get a feel :)

## Bounty Config
Current Schema:
```
{
    populationSize?: number;
    mutationPercent?: number;
    selectionStrategy?: 'tournament' | 'fitnessProportional';
    tournamentSize?: number;
}
```

