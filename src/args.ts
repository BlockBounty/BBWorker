import { usage } from 'yargs';

export const argv = usage('Usage: $0 <command> [options]')

    // 'new' Command
    .command('new', 'start a new job, returns the job id', (Argv) => {
        return Argv
            .option('f', {
                alias: 'file',
                type: 'string',
                demand: 'Please specify a wasm file for the new job',
                nargs: 1,
                describe: "The wasm file that will be ran on clients",
            })
            .option('c', {
                alias: 'config',
                type: 'string',
                demand: 'Please specify a config file for the new job',
                nargs: 1,
                describe: "The config file that will create the job",
            })
    })
    .example('$0 new -f wasm.wasm -c config.json [-s http://localhost:8089]', 'start a new job, returns the job id')

    // 'work' Command
    .command('work', 'start working on an existing job', (Argv) => {
        return Argv
            .option('j', {
                alias: 'job',
                type: 'number',
                demand: "Please specify job id with '-j'",
                nargs: 1,
                describe: 'Job id to contribute to'
            })
            .option('a', {
                alias: 'address',
                type: 'string',
                demand: "Please address to pay with '-a'",
                nargs: 1,
                describe: 'Ethereum address to pay shares of the bounty to'
            })
    })
    .example('$0 work -a 0x0 -j 1 [-s http://localhost:8089]', 'start working on an existing job')

    .demandCommand(1, 'Please specify one of the commands!')
    .strict()

    .option('s', {
        alias: 'server',
        type: 'string',
        describe: 'Optional parameter to specify the api server to use',
        default: 'http://localhost:8089'
    })

    .help('h')
    .alias('h', 'help')
    .wrap(null).argv;