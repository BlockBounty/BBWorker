import { HttpClient } from "./HttpClient";

declare const WebAssembly: any;

export class WasmService {

    private currentJobId = 0;
    private wasmExports: WasmExports;

    getCachedExports(jobId: number, apiUrl: string): Promise<WasmExports> {
        if (jobId == this.currentJobId && this.wasmExports) {
            return Promise.resolve(this.wasmExports);
        }

        this.currentJobId = jobId;

        return this.getExports(jobId, apiUrl);
    }

    getExports(jobId: number, apiUrl: string): Promise<WasmExports> {
        return HttpClient.request({
            method: 'GET',
            uri: `${apiUrl}/api/wasm/${jobId}`,
            resolveWithFullResponse: true,
            encoding: null
        }).then(response => {
            if (response.statusCode == 200) {
                return response.body;
            } else {
                throw new Error('Failed to get a job');
            }
        }).then(moduleByteCode => WebAssembly.instantiate(moduleByteCode, {
            memory: new WebAssembly.Memory({ initial: 3 })
        })).then(results => {
            this.wasmExports = results.instance.exports;
            return this.wasmExports;
        });
    }
}

export interface WasmExports {
    init(seed: number): void;
    pushFloat(n: number): void;
    pushByte(b: number): void;
    getFitness(): number;
    getSteps(): number;
}