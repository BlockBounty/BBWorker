export class ProgressService {
    evals: number = 0;

    constructor(private listener: ProgressListener) {
    }

    evalFinished() {
        this.listener(this.evals++);
    }
}

export type ProgressListener = (evals: number) => void;
