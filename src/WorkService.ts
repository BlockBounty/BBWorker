import { RequestConfig, HttpClient } from "./HttpClient";
import { WorkerConfig } from "./FitnessWorker";

export class WorkService {

    batchedWork: Work[] = [];
    batchedWorkResults: WorkResult[] = [];

    constructor(private config: WorkerConfig) { }

    public getNextWork(): Promise<Work> {
        if (this.batchedWork.length != 0) {
            return Promise.resolve(this.batchedWork.pop());
        }

        return new Promise((resolver, rejecter) => {
            this.fetchWorkBatch(resolver, rejecter, 0);
        });
    }

    public postWorkDone(result: WorkResult): Promise<void> {
        this.batchedWorkResults.push(result);
        if (this.batchedWork.length > 0) {
            return;
        }

        return HttpClient.request(this.postWorkRequestConfig())
            .then(_ => Promise.resolve());
    }

    private fetchWorkBatch(resolver: (obj: any) => void, rejecter: (obj: any) => void, failureCount: number): Promise<void> {

        return HttpClient.request(this.getJobRequestConfig())
            .then(json => {
                json.controllers.forEach((j: Work) => this.batchedWork.push(j));
                resolver(this.batchedWork.pop());
            })
            .catch(err => {
                console.log(err);
                console.log("Failed to get a job");
                if (failureCount <= 15) {
                    setTimeout(() => this.fetchWorkBatch(resolver, rejecter, ++failureCount), 1000);
                } else {
                    rejecter(`Giving up on job ${this.config.jobId} at server ${this.config.apiUrl}`);
                }
            });
    }

    private getJobRequestConfig(): RequestConfig {
        return {
            method: 'GET',
            uri: `${this.config.apiUrl}/api/controllers/${this.config.jobId}?batchSize=${this.config.batchSize}`,
            headers: {
                'X-Ether-Address': this.config.walletAddress
            },
            json: true,
        };
    }

    private postWorkRequestConfig(): RequestConfig {
        return {
            method: 'POST',
            uri: `${this.config.apiUrl}/api/fitness/${this.config.jobId}`,
            headers: {
                'X-Ether-Address': this.config.walletAddress,
                'Content-Type': 'application/json'
            },
            body: this.batchedWorkResults,
            json: true
        };
    }
}

export interface Work {
    jobId: number;
    id: number;
    seed: number;
    controller: string;
}

export interface WorkResult {
    fitness: number;
    steps: number;
    seed: number;
    controllerId: number;
}
