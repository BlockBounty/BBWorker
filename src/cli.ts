#!/usr/bin/env node
import { argv } from './args';
import { FitnessWorker } from './FitnessWorker';
import { JobMaker, JobConfig } from './JobMaker';
import { readFileSync, createReadStream } from 'fs';

const exit = (exitCode: number) => {
    console.log('\n');
    process.exit(exitCode);
};

process.on('exit', exit);
process.on('SIGINT', exit.bind(null, 0));
process.title = "bbworker";

const Spinner: any = require('clui').Spinner;

if (argv._[0] == 'work') {
    const spinner = new Spinner('Going to work', ['⣾', '⣽', '⣻', '⢿', '⡿', '⣟', '⣯', '⣷'].reverse());
    spinner.start();
    new FitnessWorker({
        apiUrl: argv.server,
        batchSize: 5,
        jobId: argv.job,
        walletAddress: argv.address,
    }, (evals) => {
        spinner.message('Fitness Evaluations: ' + evals);
    }).start();
}

if (argv._[0] == 'new') {
    JobMaker.submit(argv.server, <JobConfig>readFileSync(argv.config, "utf-8"), createReadStream(argv.file))
        .then(res => {
            console.log('Job id: ', res.jobId);
        }).catch(err => console.log("Error making new job", err));
}
