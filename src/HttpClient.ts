const isNode = typeof process === 'object';

class NodeHttpClient implements Requestor {

    private rp: any;

    constructor() {
        this.rp = require('request-promise-native');
    }

    request(options: RequestConfig): Promise<any> {
        return this.rp(options);
    }
};

class WebHttpClient implements Requestor {

    request(options: RequestConfig): Promise<any> {
        return Promise.resolve();
    }
};

export interface RequestConfig {
    method: 'POST' | 'GET';
    uri: string;
    headers?: { [header: string]: string };
    body?: any;
    formData?: any;
    resolveWithFullResponse?: boolean;
    encoding?: string;
    json?: boolean;
};

export interface Requestor {
    request(options: RequestConfig): Promise<any>;
}

export const HttpClient: Requestor = isNode ? new NodeHttpClient() : new WebHttpClient();
