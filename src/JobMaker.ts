import { HttpClient } from "./HttpClient";

export class JobMaker {
    static submit(apiUrl: string, config: JobConfig, wasm: any): Promise<any> {
        return HttpClient.request({
            method: 'POST',
            uri: `${apiUrl}/api/jobs`,
            json: true,
            formData: {
                config: config,
                file: {
                    value: wasm,
                    options: {
                        contentType: "multipart/form-data",
                    }
                }
            }
        });
    }
}

export interface JobConfig {
    populationSize?: number;
    mutationPercent?: number;
    selectionStrategy?: 'tournament' | 'fitnessProportional';
    tournamentSize?: number;
}
