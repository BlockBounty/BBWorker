import { RequestConfig, HttpClient } from "./HttpClient";
import { WorkService, WorkResult } from "./WorkService";
import { WasmService, WasmExports } from "./WasmService";
import { ProgressService, ProgressListener } from './ProgressService';

export class FitnessWorker {

    batchedJob: number[] = [];
    evaluations = 0;
    apiService: WorkService;
    wasmService: WasmService;
    progressService: ProgressService;

    constructor(private config: WorkerConfig, progressListener: ProgressListener) {
        this.apiService = new WorkService(config);
        this.wasmService = new WasmService();
        this.progressService = new ProgressService(progressListener);
    };

    start() {
        this.apiService.getNextWork()
            .then(work => Promise.all([
                this.wasmService.getExports(this.config.jobId, this.config.apiUrl),
                Promise.resolve({ controller: work.controller, seed: work.seed, controllerId: work.id, jobId: work.jobId })
            ])).then(([wasmExports, work]) => {
                this.pushController(work.controller, wasmExports);
                wasmExports.init(work.seed);
                return Promise.resolve(<WorkResult>{
                    fitness: wasmExports.getFitness(),
                    steps: wasmExports.getSteps(),
                    seed: work.seed,
                    controllerId: work.controllerId
                });
            }).then(doneWork => {
                this.progressService.evalFinished();
                return this.apiService.postWorkDone(doneWork)
            })
            .then(() => this.start())
            .catch(err => console.log(err));

    }

    private pushController(controller: string, wasmExports: WasmExports) {
        controller.split(' ').forEach(c => {
            if (!isNaN(<any>c)) {
                wasmExports.pushFloat(Number(c));
            } else {
                wasmExports.pushByte(c.charCodeAt(0));
            }
        });

        wasmExports.pushByte('='.charCodeAt(0));
    }

}

export interface WorkerConfig {
    apiUrl: string;
    walletAddress: string;
    batchSize: number;
    jobId: number;
}
